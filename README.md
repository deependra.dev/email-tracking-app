# Email Tracking App

### Task description:
```
Email Tracking
Introduction
Vested needs to handle events coming from a third-party API to track the state of outreach messages sent through our hiring platform.

Your goal is to build a services that exposes one or more API endpoints to handle events and display statistics about the outreach status at any point in time.

Workflow
We don't want you to be a code monkey, some of the requirements for this exercise are not 100% clear and that's intended. We want to understand your assumptions and approaches you've taken during the implementation. If you have questions, don't hesitate to ask.
Your commit history is important, we want to know the steps you've taken throughout the process, make sure you don't commit everything at once.
In the README.md of your project, explain what conclusions you've made form the entities, constraints, requirements and use cases of this exercise.
Entities
Email --- Match --- Job
  |       |
  |       |
Event   Candidate
A Job has a title and a state (open, closed).
A Candidate has a name, email and the date it was added to the platform.
A Match has an outreach state (in progress, replied, etc.), a candidate interest (interested, not interested, etc.) and the date the match was created.
A Match is associated to both a Job and a Candidate uniquely identified by candidate and job.
An Email has the date it was sent.
An Email is created when a message.created event is received.
An Email is marked as bounced when a message.bounced event is received.
An Email is marked as responded when a message.replied event is received.
An Event has a date, a type and some metadata information related to the event.
An Event is associated to an Email.
Example of Events
Message Created

{
    "date": 1471630848,
    "type": "message.created",
    "metadata": {
        "message_id": "cjld2cjxh0000qzrmn831i7rn",
        "match_id": 123,  
    }
}
Message Replied

{
    "date": 1471630848,
    "type": "message.replied",
    "metadata": {
        "message_id": "cjld2cjxh0000qzrmn831i7rn",
    }
}
Message Bounced

{
    "date": 1471630848,
    "type": "message.bounced",
    "metadata": {
        "message_id": "cjld2cyuq0000t3rmniod1foy",
    }
}

Constraints and Requirements
The service must be implemented in Python with Django and PostgreSQL.
The service must be dockerized.
Share a public Gitlab repository when you are done.
The service will be receiving, in average, ~100 events/second.

Use Cases
Your service must provide and endpoint to query events from:
A specific Candidate.
A specific Job.
A specific time range for a Job.

The backend team must be able to monitor errors that occur while processing the events, for example:
An event has a date in the future.
An event is sending unexpected/invalid value in the metadata.

Pluses
Your API is documented.
```

## Project Setup:
First take clone from repository given below:
https://gitlab.com/deependra.dev/email-tracking-app.git

```
git clone https://gitlab.com/deependra.dev/email-tracking-app.git
```

Install python 3.8 or any latest version of python and Intall docker with latest version.

goto the project directory where <docker-compose.yml> file exists.
and run below commands:

Run Build:
```
docker-compose -f docker-compose.yml up --build
```

Down docker container:
```
docker-compose -f docker-compose.yml down
```

Remove unused Database:
```
docker system prune --volumes -f
```

### Create Candidate
URL:
http://0.0.0.0:8000/api/candidate

Method: POST

Payload:
``` 
{
    "name": "test user",
    "email": "testuser@gmail.com"
}
```

### Update or delete Candidate using id
URL:
http://0.0.0.0:8000/api/candidate/update-delete/1

Method: POST or DELETE

Payload: 
```
{
   "name": "test user update",
   "email": "testuser@gmail.com"
}
```


### Create a new Job
URL: 
http://0.0.0.0:8000/api/job/

Method: POST

Payload:
```
{
    "name": "Software Engineer",
    "state": "open"
}
```


### Update or Delete Existing Job
URL: 
http://0.0.0.0:8000/api/job/update-delete/1

Method: POST or DELETE

Payload:
```
{
    "name": "Software Engineer",
    "state": "close"
}
```

### Create New Match
URL: 
http://0.0.0.0:8000/api/match/

Method: POST

Payload:
```
{
    "outreachState": "in progress",
    "candidateInterest": "interested",
    "jobs": 1,
    "candidate": 1
}
```

Response:
```
{
    "id": 1,
    "date": "2022-05-18T12:41:43.338890Z",
    "outreachState": "in progress",
    "candidateInterest": "interested",
    "jobs": 1,
    "candidate": 1
}
```


### Update or Delete Match
URL: 
http://0.0.0.0:8000/api/match/update-delete/1

Method: POST or DELETE

Payload:
```
{
    "outreachState": "replied",
    "candidateInterest": "interested",
    "jobs": 1,
    "candidate": 1
}
```

Response:
```
{
    "id": 1,
    "date": "2022-05-18T12:44:57.168617Z",
    "outreachState": "replied",
    "candidateInterest": "interested",
    "jobs": 1,
    "candidate": 1
}
```

### List the Events
URL: http://0.0.0.0:8000/api/event/

Method: GET


### Create Events
URL: http://0.0.0.0:8000/api/event/create/

Method: POST

Payload:
```
{
    "date": 1471630848,
    "type": "message.created",
    "metadata": {
        "message_id": "cjld2cjxh0000qzrmn831i7rn",
        "match_id": 123,  
    }
}
```

### Run Service for Add Event from third party APIs
URL: http://0.0.0.0:8000/service/email_service?url="<Third party APIs>"

Method: GET


### Run Service for Filter Events based on Candidate, Job
URL: http://0.0.0.0:8000/service/filter/events?candidate=1

Method: GET


### Fitler For Job
URL: http://0.0.0.0:8000/service/filter/events?job=1

Method: GET
