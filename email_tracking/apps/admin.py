from django.contrib import admin
from .models import Candidate, Job, Match, Event, Email

# Register your models here.


admin.site.register(Candidate)
admin.site.register(Job)
admin.site.register(Match)
admin.site.register(Event)
admin.site.register(Email)
