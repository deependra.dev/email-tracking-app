from rest_framework.exceptions import ValidationError
from datetime import datetime
from apps.models import Match, Email
from apps.serializers import EmailSerializer


def convert_into_datetime(value):
    """
    convert string into datetime
    """
    try:
        return datetime.strptime(value, "%d-%m-%y %H:%M:%S")
    except Exception:
        raise ValidationError("date-time not valid")


def check_valid_metadata(value):
    """
    check given message_id and match_id is valid or not to create event
    """
    try:
        if "match_id" and "message_id" in value:
            if Match.objects.filter(pk=value["match_id"]):
                return value
            raise ValidationError("unexpected/invalid metadatas")
    except:
        raise ValidationError("unexpected/invalid metadatas")


def check_valid_data(data):
    """
    handle convert_into_datetime and check_valid_metadata function
    """
    try:
        data["date"] = convert_into_datetime(data["date"])
        data["metadata"] = check_valid_metadata(data["metadata"])
        return data
    except:
        raise ValidationError("unexpected/invalid metadata")


def email_add_update(data):
    """
    it will update Email bassed on the event type
        1) if event type is created then it will create Email
        2) if event type is replied then update Email's status as replied
        3) if event type is bounced then update Email's status as bounced

    """
    if data["type"] == "created":
        serializers = EmailSerializer(data={"status": "created", "event": data["id"]})
        if serializers.is_valid():
            serializers.save()
    elif data["type"] == "replied":

        if Email.objects.filter(status="created", event=data["id"]):
            Email.objects.filter(status="created", event=data["id"]).update(
                staus="replied"
            )
        else:
            serializers = EmailSerializer(
                data={"status": "replied", "event": data["id"]}
            )
            if serializers.is_valid():
                serializers.save()
    elif data["type"] == "bounced":
        if Email.objects.filter(status="created", event=data["id"]):
            Email.objects.filter(status="created", event=data["id"]).update(
                staus="bounced"
            )
        else:
            serializers = EmailSerializer(
                data={"status": "bounced", "event": data["id"]}
            )
            if serializers.is_valid():
                serializers.save()
