from django.http import JsonResponse
from rest_framework.views import APIView
from rest_framework.generics import GenericAPIView
from rest_framework.mixins import (
    ListModelMixin,
    CreateModelMixin,
    RetrieveModelMixin,
    UpdateModelMixin,
    DestroyModelMixin,
)
from apps.models import (
    Candidate,
    Job,
    Match,
    Event,
)
from apps.serializers import (
    CandidateSerializer,
    JobSerializer,
    MatchSerializer,
    EventSerializer,
)
from apps.service import (
    check_valid_data,
    email_add_update,
)


class CandidateAddList(GenericAPIView, ListModelMixin, CreateModelMixin):
    """
    this api add and list candidate's value by using generic and mixin
    """

    serializer_class = CandidateSerializer
    queryset = Candidate.objects.all()

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class CandidateListDeleteUpdate(
    GenericAPIView, RetrieveModelMixin, UpdateModelMixin, DestroyModelMixin
):
    """
    this api retrive, update and delete candidate's value by
    using generic and mixin
    """

    serializer_class = CandidateSerializer
    queryset = Candidate.objects.all()

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class JobAddList(GenericAPIView, ListModelMixin, CreateModelMixin):
    """
    this api add and list job's value by using generic and mixin
    """

    serializer_class = JobSerializer
    queryset = Job.objects.all()

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class JobListDeletUpdate(
    GenericAPIView, RetrieveModelMixin, UpdateModelMixin, DestroyModelMixin
):
    """
    this api retrive, update and delete job's value by
    using generic and mixin
    """

    serializer_class = JobSerializer
    queryset = Job.objects.all()

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class MatchAddList(GenericAPIView, ListModelMixin, CreateModelMixin):
    """
    this api add and list match's value by using generic and mixin
    """

    serializer_class = MatchSerializer
    queryset = Match.objects.all()

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class MatchListDeletUpdate(
    GenericAPIView, RetrieveModelMixin, UpdateModelMixin, DestroyModelMixin
):
    """
    this api retrive, update and delete match's value by
    using generic and mixin
    """

    serializer_class = MatchSerializer
    queryset = Match.objects.all()

    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)


class EventList(GenericAPIView, ListModelMixin, RetrieveModelMixin):
    """
    this api list and retrive event's value by using generic and mixin
    """

    serializer_class = EventSerializer
    queryset = Event.objects.all()

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class EventAdd(APIView):
    """
    this api handle to create event And create or update email
    """

    def post(self, request):
        data = check_valid_data(request.data)
        serializers = EventSerializer(data=data)
        if serializers.is_valid():
            serializers.save()
            email_add_update(serializers.data)
            return JsonResponse({"message": serializers.data}, safe=False, status=200)
        return JsonResponse({"message": serializers.errors}, safe=False, status=500)
