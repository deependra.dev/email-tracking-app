from django.db import models
from django.utils import timezone
from django.core.exceptions import ValidationError


def validate_date(date):
    if date > timezone.now():
        raise ValidationError("Date cannot be in the future")


class DateTimeValue(models.Model):
    """
    this abstract model represent current date and time
    """

    date = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Candidate(DateTimeValue):
    """
    this model represent candidate attributes

    Baseclass is DateTimeValue for  datetime
    """

    name = models.CharField(max_length=50)
    email = models.EmailField()

    def __str__(self):
        return self.name


class Job(models.Model):
    """
    this model represent job attributes
    """

    state_choice = (
        ("open", "open"),
        ("close", "close")
        )
    name = models.CharField(max_length=50)
    state = models.CharField(choices=state_choice, max_length=10, default="open")

    def __str__(self):
        return self.name


class Match(DateTimeValue):
    """
    this model represent match attributes for job and candidate
    Baseclass is DateTimeValue for  datetime
    """

    outreach_State = (
        ("in progress", "in progress"),
        ("replied", "replied")
    )
    
    candidate_interest = (
        ("interested", "interested"),
        ("not interested", "not interested"),
    )
    outreachState = models.CharField(max_length=20, choices=outreach_State)
    candidateInterest = models.CharField(max_length=20, choices=candidate_interest)
    jobs = models.ForeignKey(Job, on_delete=models.CASCADE)
    candidate = models.ForeignKey(Candidate, on_delete=models.CASCADE)

    def __str__(self):
        return self.outreachState


class Event(models.Model):
    """
    this model represent event attributes
    """

    type_choice = (
        ("created", "created"),
        ("replied", "replied"),
        ("bounced", "bounced"),
    )
    date = models.DateTimeField(null=True, blank=True, validators=[validate_date])
    type = models.CharField(max_length=50, choices=type_choice)
    metadata = models.JSONField()

    def __str__(self):
        return self.type

    def validate_date(date):
        if date > timezone.now():
            raise ValidationError("Date cannot be in the future")


class Email(DateTimeValue):
    status = models.CharField(max_length=10, blank=True, null=True)
    event = models.ForeignKey(Event, on_delete=models.CASCADE)

    def __str__(self):
        return self.status
