from django.urls import path
from apps import views


urlpatterns = [
    path(
        "api/candidate/",
        views.CandidateAddList.as_view(),
        name="candidate-create-and-list",
    ),
    path(
        "api/candidate/update-delete/<int:pk>",
        views.CandidateListDeleteUpdate.as_view(),
        name="candidate-update-and-delete",
    ),
    path("api/job/", views.JobAddList.as_view(), name="job-create-and-list"),
    path(
        "api/job/update-delete/<int:pk>",
        views.JobListDeletUpdate.as_view(),
        name="job-update-and-delete",
    ),
    path("api/match/", views.MatchAddList.as_view(), name="match-create-and-list"),
    path(
        "api/match/update-delete/<int:pk>",
        views.MatchListDeletUpdate.as_view(),
        name="match-update-and-delete",
    ),
    path("api/event/", views.EventList.as_view(), name="event-list"),
    path("api/event/create/", views.EventAdd.as_view(), name="event-create"),
]
