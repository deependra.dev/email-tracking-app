from django.urls import path
from . import views

urlpatterns = [
    path("email_service", views.EmailService.as_view(), name="email_service"),
    path("filter/events",
        views.FilterEvents.as_view(),
        name="filter-events",
    ),
]
