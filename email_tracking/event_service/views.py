from django.http import JsonResponse
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from apps.models import Job, Candidate, Match, Event
import requests


class EmailService(APIView):
    def get(self, request):
        try:
            third_party_url = request.query_params.get('url', None)
            if third_party_url:
                third_party_response = requests.get(third_party_url).json()
                if "type" and "metadata" and "date" in third_party_response:
                    response = requests.post(
                        "http://127.0.0.1:8000/event/add-api/",
                        third_party_response,
                        format='json',
                    )
                    if response:
                        return Response({"success": "Event Added Successfully!!!"})
                    else:
                        return Response({"error": "Event Cannot be Created"})
                else:
                    return Response(
                        {
                            "error": "Enter valid url",
                            "status": status.HTTP_500_INTERNAL_SERVER_ERROR
                        }
                    )
            else:
                return Response(
                    {
                        "error": "Enter a valid url",
                        "status": status.HTTP_500_INTERNAL_SERVER_ERROR
                    }
                )
        except Exception as e:
            return JsonResponse({"message": e}, safe=False, status=500)


class FilterEvents(APIView):
    def get(self, request):
        candidate = request.query_params.get('candidate', None)
        job = request.query_params.get('job', None)
        
        if candidate:
            candidates = Candidate.objects.filter(id=candidate)
            if candidates:
                match_filter = Match.objects.filter(
                    candidate=[c.id for c in Candidate.objects.filter(id=candidate)][0]
                )[::1]
                all_event = list(Event.objects.all())
                event_data = []
                if match_filter:
                    for events in all_event:
                        if events.metadata["match_id"] == str(
                            [m.id for m in match_filter][0]
                        ):
                            event_data.append(
                                {
                                    "id": events.id,
                                    "date": events.date,
                                    "type": events.type,
                                    "metadata": events.metadata,
                                }
                            )
                return Response(event_data, status.HTTP_200_OK)
            else:
                return Response("Invalid Candidate", status.HTTP_204_NO_CONTENT)
        elif job:
            jobs = Job.objects.filter(id=job)
            if jobs:
                match_filter = Match.objects.filter(
                    jobs=[j.id for j in Job.objects.filter(id=job)][0]
                )[::1]
                all_event = list(Event.objects.all())
                event_data = []
                if match_filter:
                    for events in all_event:
                        if events.metadata["match_id"] == str(
                            [m.id for m in match_filter][0]
                        ):
                            event_data.append(
                                {
                                    "id": events.id,
                                    "date": events.date,
                                    "type": events.type,
                                    "metadata": events.metadata,
                                }
                            )
                return Response(event_data, status.HTTP_200_OK)
            else:
                return Response("Invalid Job", status.HTTP_204_NO_CONTENT)
        else:
            return Response("Please provide job or candidate", status.HTTP_204_NO_CONTENT)
